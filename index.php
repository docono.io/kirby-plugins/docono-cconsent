<?php

/**
 * DOCONO
 *
 * @author Renzo Mueller <renzo@docono.io>
 * @copyright Copyright (c) DOCONO (https://docono.io)
 */

load([
    'docono\\CConsent\\Exception' => 'CConsent/Exception.php',
    'docono\\CConsent\\CConsent' => 'CConsent/CConsent.php',
    'docono\\CConsent\\ConsentHandler' => 'CConsent/ConsentHandler.php',
    'docono\\CConsent\\AbstractConsentHandlerDependency' => 'CConsent/AbstractConsentHandlerDependency.php',
    'docono\\CConsent\\Slot' => 'CConsent/Slot.php',
    'docono\\CConsent\\JavaScript' => 'CConsent/JavaScript.php',
    'docono\\CConsent\\Youtube' => 'CConsent/Youtube.php',
    'docono\\CConsent\\Youtube\\Thumbnail' => 'CConsent/Youtube/Thumbnail.php'
], __DIR__);

Kirby::plugin('docono/cconsent', [

]);

/**
 * register tpl function
 * @return \docono\CConsent\CConsent
 */
function dcc() {
    return \docono\CConsent\CConsent::getInstance();
}