<?php

/**
 * DOCONO
 *
 * @author Renzo Mueller <renzo@docono.io>
 * @copyright Copyright (c) DOCONO (https://docono.io)
 */

namespace docono\CConsent;

use docono\CConsent\Youtube\Thumbnail;
use Kirby\Http\Uri;

class Youtube extends AbstractConsentHandlerDependency
{
    /**
     * @var string
     */
    private string $url;

    /**
     * @param string $url
     * @param string $message
     */
    public function __construct(string $url)
    {
        parent::__construct();

        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getVideoId(): string
    {
        return (new Uri($this->url))->path()->last();
    }

    /**
     * @return string
     */
    public function getBase64Thumbnail(string $thumbnailQuality): string
    {
        return Thumbnail::instance()->getBase64ThumbnailForId($this->getVideoId(), $thumbnailQuality);
    }

    /**
     * @return string
     */
    public function getHtml(string $thumbnailQuality = 'high', array $attr=[]): string
    {
        $classes = 'dcc-youtube';

        if(!empty($attr['class'])) {
            $attr['class'] .=  ' ' . $classes;
        } else {
            $attr['class'] = $classes;
        }

        if ($this->consentHandler()->youtubePermission()) {
            // use kirby youtube tag
            return youtube($this->url, [], $attr);
        } else {
            try {
                $attributes = join(' ', array_map(function($key) use ($attr){
                    if(is_bool($attr[$key])){
                        return $attr[$key]?$key:'';
                    }
                    return $key.'="'.$attr[$key].'"';
                }, array_keys($attr)));

                return '<div data-src="' . $this->url . '" ' . $attributes . '><img class="dcc-youtube__thumbnail" src="' . $this->getBase64Thumbnail($thumbnailQuality) . '" /><div class="dcc-youtube__play"></div><div class="dcc-youtube__consent"><div>' . t('dcc.youtube.message', 'Youtube Cookies annehmen?') . '</div><div><button class="dcc-button consent-youtube__decline">' . t('dcc.youtube.decline', 'ablehnen'). '</button><button class="dcc-button -selected consent-youtube__accept">' . t('dcc.youtube.accept', 'annehmen'). '</button></div></div></div>';
            } catch (\Throwable $e) {
                return '';
            }
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getHtml();
    }
}