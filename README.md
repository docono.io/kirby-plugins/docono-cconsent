<h1 align="center">Docono Cookie Consent</h1>

## setup
add Guzzle with composer

```
composer require guzzlehttp/guzzle
```

load dcc js file (ensure it is loaded aysnc => kirby function js with second param true)

```php
<?= js(dcc()->jsFile(), true); ?>
```

there is also a basic styling if you want
```php
<?= css(dcc()->cssFile()); ?>
```
css vars:
```
  --dcc-clr-white: #FBFAF5;
  --dcc-clr-black: #2A363B;
  --dcc-clr-green: #99B898;

  --dcc-body-w: 50rem;
  --dcc-gap: 2rem;
  --dcc-my: 3rem;
  --dcc-mx: auto;

  --dcc-yt-width: 50rem;
  --dcc-yt-ratio: 16/9;

  --dcc-switch-size: 1.5rem;
```

extend your config file with the desired permissions/consents

```php
    'docono.cconsent' => [
        'permissions' => ['analytics', 'gmaps', 'youtube']
    ]
```

## consent dialog

Create your consent dialog whatever you like it.
Just ensure:
- the main container has the id "dc-consent"
- the "accept essential" button has the id "accept-essential"
- the "accept selected consents" button has the id "accept-selected"
- the name of the checkbox response with the defined consent


```php
<?php dcc()->dialog(); ?>
<div id="dc-consent" data-ttl="1000">
    <div class="dcc-body">
        <h3>Wir schenken Cookies!</h3>
        <p>
            Möchtest du unsere leckeren Cookies?
            Ansonsten wähle dir die Cookies welche dir am besten schmecken.
        </p>

        <div class="dcc-consents">
            <label>
                <div class="dcc-slider">
                    <input type="checkbox" name="essential" checked disabled>
                    <div class="slider"></div>
                </div>


                Technische cookies
            </label>

            <?php foreach (dcc()->permissionList() as $permission): ?>
                <label>
                    <div class="dcc-slider">
                        <input type="checkbox" checked name="<?= $permission ?>">
                        <div class="slider"></div>
                    </div>

                    <?= $permission ?>
                </label>
            <?php endforeach; ?>
        </div>


        <div class="dcc-actions">
            <button id="accept-essential" class="dcc-button -inverted">essentielle</button>
            <button id="accept-selected" class="dcc-button -selected">ausgewählte</button>
        </div>
    </div>
</div>
<?= dcc()->endDialog(); ?>
```

## JavaScript

To prevent any JavaScript from execution, such as google analytics, use the JS functionality:
Set the "data-consent" to the required consent name.

```php
<?php dcc()->js()->start(); ?>
<script
        data-consent="analytics"
        src="https://www.googletagmanager.com/gtag/js?id=UA-xxxxx"
        async
></script>
<?= dcc()->js()->end(); ?>
```
The "src" attribute will be replaced with "data-src".
Is the consent given, the script will be loaded.

or

```php
<?php dcc()->js()->start(); ?>
<script data-consent="analytics" type="text/javascript">
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments)
    }

    gtag('consent', 'default', {ad_storage: 'denied', analytics_storage: 'denied'});
    gtag('set', 'ads_data_redaction', true);
    gtag('set', 'url_passthrough', true);
    gtag('js', new Date());
    gtag('config', 'xxxx');
</script>
```

The script "type" attribute value will be replaced with "text/plain", which prevents the script of being executed.
If the consent is given, the script tag will be executed.

## Youtube
With the youtube helper, you ensure that no call will be made to youtube from the client browser.
If the the consent "youtube" is not accepted, a placeholder with a base64 thumbnail will be shown.
Is the thumbnail clicked, a consent prompt for that video will be shown.

### styling
- container: .dcc-youtube
- play icon: .dcc-youtube__play
- consent prompt: .dcc-youtube__consent
- consent prompt buttons: .consent-youtube__decline, .consent-youtube__accept

### translations
- dcc.youtube.message
- dcc.youtube.decline
- dcc.youtube.accept


```php
<?= dcc()->youtube('https://www.youtube-nocookie.com/embed/oyMwn-gWcfg?controls=0') ?>
```

or get html tag with ```getHtml(thumbnailQuality='high', attributes=[])```

```php
<?= dcc()->youtube('https://www.youtube-nocookie.com/embed/oyMwn-gWcfg?controls=0')->getHtml('low', ['class' => 'my-youtube-video']) ?>
```

## Slots

If you want to show any content only if the consent is accepted, then there are slots

```php
<?php dcc()->slot('mySlot')->start('requiredConsent'); ?>
<div>
    content if 'requiredConsent' is accepted
</div>
<?= dcc()->slot('mySlot')->end(); ?>
```